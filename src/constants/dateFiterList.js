export const dateFilterList = [
  "Today",
  "Yesterday",
  "Last 7 days",
  "Last 30 days",
  "Last Month",
  "This Year",
  "Lifetime",
];
