import { createTheme } from "@material-ui/core/styles";

const useMuiTheme = () => {
  return createTheme({
    palette: {
      primary: {
        main: "#efa413",
      },
    },
    // breakpoints: {
    //   values: {
    //     xs: 0,
    //     sm: 768,
    //     md: 960,
    //     lg: 1280,
    //     xl: 1920,
    //   },
    // },
    // overrides: {
    //   MuiIconButton: {
    //     root: {
    //       padding: "20px",
    //     },
    //   },
    // },
    components: {
      MuiIconButton: {
        styleOverrides: {
          root: {
            padding: 0,
          },
        },
      },
    },
  });
};

export { useMuiTheme };
