import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import { useMuiTheme } from "./themes";
import { DatePicker } from "./components/DatePicker";
import { Header } from "./components/Header";

const App = () => {
  const theme = useMuiTheme();
  return (
    <ThemeProvider theme={theme}>
      <Header />
      <DatePicker />
    </ThemeProvider>
  );
};

export default App;
