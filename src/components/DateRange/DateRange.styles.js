import { createStyles, makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "flex",
      padding: theme.spacing(1, 2),
      backgroundColor: "#efa413",
      borderRadius: 0,

      "& .MuiOutlinedInput-notchedOutline": {
        borderWidth: 0,
      },

      "& .MuiDateRangePickerDay-dayInsideRangeInterval ": {
        color: "#fff",
      },

      "& .MuiDateRangePickerDay-day": {
        //color: "#fff",
      },
      "& .MuiDateRangePickerDay-root": {
        borderRadius: 0,
      },
      "& .MuiDateRangePickerDay-rangeIntervalDayHighlight": {
        backgroundColor: "#efa413",
        color: "#fff",
      },

      "& .MuiDateRangePickerDay-root.Mui-selected": {
        backgroundColor: "#efa413",
        color: "#fff",
      },

      "& .MuiDateRangePickerDay-root:last-of-type, & .MuiDateRangePickerDay-root:first-of-type":
        {
          borderRadius: 0,
        },
      "& .MuiDateRangePickerDay-day.Mui-selected": {
        borderRadius: 0,
        backgroundColor: "#efa413",
      },

      "& .MuiDateRangePickerDay-day:not(.Mui-selected)": {
        border: "none",
      },

      "& .MuiDateRangePickerDay-day.Mui-selected:hover,& .MuiDateRangePickerDay-day.Mui-selected:focus  ":
        {
          backgroundColor: "#efa413",
        },
      "& .MuiListItem-root.Mui-selected": {
        backgroundColor: "#efa413",
        color: "#fff",
      },
      "& .MuiListItem-root": {
        color: "#000",
      },
    },

    hide: {
      visibility: "hidden",
    },
    filterList: {
      display: "flex",
      flexDirection: "column",
    },
  })
);
