import React from "react";
import { useDispatch, useSelector } from "react-redux";
import * as moment from "moment";
import clsx from "clsx";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import StaticDateRangePicker from "@mui/lab/StaticDateRangePicker";
import { useStyles } from "./DateRange.styles";
import { Divider } from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import {
  setDateFilter,
  setHideSelected,
  setCurrentRangeStart,
  setCurrentRangeEnd,
  updateInitDate,
} from "../../actions/dateRangeActions";
import { dateFilterList } from "../../constants/dateFiterList";

export const DateRange = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { today, lifetime, initDate, hideSelected, dateFilter } = useSelector(
    (state) => state
  );

  const setFilter = (filter) => {
    switch (filter) {
      case "Today":
        dispatch(setCurrentRangeStart(new Date()));
        dispatch(setCurrentRangeEnd(new Date()));
        break;

      case "Yesterday":
        dispatch(
          setCurrentRangeStart(moment(new Date()).subtract(1, "days")["_d"])
        );
        dispatch(
          setCurrentRangeEnd(moment(new Date()).subtract(1, "days")["_d"])
        );
        break;

      case "Last 7 days":
        dispatch(
          setCurrentRangeStart(moment(new Date()).subtract(6, "days")["_d"])
        );
        dispatch(setCurrentRangeEnd(moment(new Date())["_d"]));

        break;

      case "Last 30 days":
        dispatch(
          setCurrentRangeStart(moment(new Date()).subtract(29, "days")["_d"])
        );
        dispatch(setCurrentRangeEnd(new Date()));
        break;

      case "Last Month":
        let month = new Date().getMonth();
        let yearM = new Date().getFullYear();
        dispatch(setCurrentRangeStart(new Date(yearM, month - 1, 1)));
        dispatch(setCurrentRangeEnd(new Date(yearM, month, 0)));
        break;

      case "This Year":
        let year = new Date().getFullYear();
        dispatch(setCurrentRangeStart(new Date(`${year}-01-01`)));
        dispatch(setCurrentRangeEnd(new Date()));
        break;

      case "Lifetime":
        dispatch(setCurrentRangeStart(lifetime));
        dispatch(setCurrentRangeEnd(new Date()));
        break;

      default:
        console.log("Нет таких значений");
    }
    dispatch(setDateFilter(filter));
    dispatch(updateInitDate());
    dispatch(setHideSelected(true));
  };

  const getCurrentRange = () => {
    dispatch(updateInitDate());
    dispatch(setHideSelected(true));
  };

  return (
    <Paper
      className={clsx(classes.root, `${hideSelected ? classes.hide : ""}`)}
    >
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <StaticDateRangePicker
          displayStaticWrapperAs="desktop"
          label="date range"
          minDate={lifetime}
          maxDate={today}
          value={[initDate.start, initDate.end]}
          onChange={(newValue) => {
            dispatch(setDateFilter(null));
            dispatch(setCurrentRangeStart(newValue[0]));
            dispatch(setCurrentRangeEnd(newValue[1]));
          }}
          onAccept={() => getCurrentRange()}
          renderInput={(startProps, endProps) => {
            return (
              <React.Fragment>
                <TextField {...startProps} />
                <Box sx={{ mx: 2 }}> to </Box>
                <TextField {...endProps} />
              </React.Fragment>
            );
          }}
        />
      </LocalizationProvider>
      <Divider variant="fullWidth" orientation="vertical" flexItem />
      <List className={classes.filterList}>
        {dateFilterList.map((filter) => {
          return (
            <ListItem
              dense={true}
              selected={filter === dateFilter}
              onClick={() => {
                setFilter(filter);
              }}
              key={filter}
            >
              <ListItemButton>
                <ListItemText primary={filter} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
    </Paper>
  );
};
