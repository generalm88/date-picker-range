import { createStyles, makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      overflow: "hidden",
      padding: theme.spacing(1, 2),
      maxWidth: "100%",
      background: "linear-gradient(135deg, #ef7401, #efa413)",
      color: "#fff",
    },
  })
);
