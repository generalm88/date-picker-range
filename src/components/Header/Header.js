import React from "react";
import { Avatar, Grid, Typography, Button } from "@material-ui/core";
import { useStyles } from "./Header.styles";

export const Header = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Button variant="contained">Go to...</Button>
        <Typography>seller metrix</Typography>
        <div>
          <Avatar>A</Avatar>
          <Typography variantMapping={{ subtitle1: "h6" }} gutterBottom>
            name
          </Typography>
        </div>
      </Grid>
    </div>
  );
};
