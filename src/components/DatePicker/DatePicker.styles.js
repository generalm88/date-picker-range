import { createStyles, makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
      alignItems: "center",
      paddingRight: theme.spacing(2),
    },
    button: {},
    hide: {
      visibility: "hidden",
    },
    filters: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "stretch",
      alignItems: "center",
      fontWeight: 500,
      padding: theme.spacing(1),
    },
    dateRange: {
      backgroundColor: "red",
    },
  })
);
