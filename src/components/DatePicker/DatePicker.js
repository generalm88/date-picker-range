import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as moment from "moment";
import IconButton from "@mui/material/IconButton";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import DateRangeIcon from "@mui/icons-material/DateRange";
import { Button, Grid } from "@mui/material";
import { DateRange } from "../DateRange";
import { convertDatePicker } from "../../utils/ConvertdatePicker";
import {
  setHideSelected,
  setEndDay,
  setStartDay,
} from "../../actions/dateRangeActions";
import { useStyles } from "./DatePicker.styles";

export const DatePicker = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const hideSelect = useSelector((state) => state.hideSelected);
  const { today, initDate, lifetime, dateFilter } = useSelector(
    (state) => state
  );

  const start = moment(initDate.start);
  const end = moment(initDate.end);
  const lifitimeM = moment(lifetime);
  const diffPeriod = end.diff(start, "days");
  const diffLimitPrev = start.diff(lifitimeM, "days");

  let todayM = moment(today);
  let diffLimitNext = todayM.diff(end, "days");

  const [isArrowRightDisabled, setIsArrowRightDisabled] = useState(true);
  const [isArrowleftDisabled, setIsArrowleftDisabled] = useState(false);

  useEffect(() => {
    if (
      (diffLimitNext >= diffPeriod && diffPeriod !== 0) ||
      diffLimitNext !== 0
    ) {
      setIsArrowRightDisabled(false);
    } else {
      setIsArrowRightDisabled(true);
    }
  }, [diffPeriod, diffLimitNext]);

  useEffect(() => {
    if (diffLimitPrev === 0 || diffPeriod >= diffLimitPrev) {
      setIsArrowleftDisabled(true);
    } else {
      setIsArrowleftDisabled(false);
    }
  }, [diffLimitPrev, diffPeriod]);

  const getPrevTime = () => {
    if (diffLimitPrev >= diffPeriod) {
      if (dateFilter === "Last Month") {
        dispatch(setEndDay(end.subtract(1, "month").endOf("month")["_d"]));
        dispatch(
          setStartDay(start.subtract(1, "month").startOf("month")["_d"])
        );
        return;
      }
      if (dateFilter === "This Year") {
        dispatch(setEndDay(end.add(-1, "year").endOf("year")["_d"]));
        dispatch(setStartDay(start.add(-1, "year").startOf("year")["_d"]));
        return;
      }
      dispatch(setEndDay(end.subtract(diffPeriod + 1, "days")["_d"]));
      dispatch(setStartDay(start.subtract(diffPeriod + 1, "days")["_d"]));
    }
  };

  const getNextTime = () => {
    let start = moment(initDate.start);
    let end = moment(initDate.end);
    let todayM = moment(today);
    let diffPeriod = end.diff(start, "days");
    let diffLimitNext = todayM.diff(end, "days");

    if (diffLimitNext >= diffPeriod) {
      if (dateFilter === "Last Month") {
        dispatch(setEndDay(end.add(1, "month").endOf("month")["_d"]));
        dispatch(setStartDay(start.add(1, "month").startOf("month")["_d"]));
        return;
      }
      if (dateFilter === "This Year") {
        dispatch(setEndDay(end.add(1, "year").endOf("year")["_d"]));
        dispatch(setStartDay(start.add(1, "year").startOf("year")["_d"]));
        return;
      }
      dispatch(setStartDay(start.add(diffPeriod + 1, "days")["_d"]));
      dispatch(setEndDay(end.add(diffPeriod + 1, "days")["_d"]));
    }
  };

  return (
    <Grid container justifyContent="flex-end">
      <Grid container justifyContent="flex-end">
        <div
          onClick={() => dispatch(setHideSelected(!hideSelect))}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
          }}
        >
          <Button>
            <IconButton aria-label="upload picture" component="span">
              <DateRangeIcon />
            </IconButton>
          </Button>
          <div
            style={{
              cursor: "pointer",
            }}
          >
            <span>{convertDatePicker(initDate, today)}</span>
          </div>
        </div>
        <div className={classes.container}>
          <IconButton
            onClick={() => getPrevTime()}
            disabled={isArrowleftDisabled}
            className={classes.button}
            color="inherit"
            aria-label="move date back"
            component="span"
          >
            <KeyboardArrowLeftIcon />
          </IconButton>

          <IconButton
            onClick={() => getNextTime()}
            disabled={isArrowRightDisabled}
            className={classes.button}
            color="inherit"
            aria-label="move date forward"
            component="span"
          >
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
      </Grid>
      <DateRange />
    </Grid>
  );
};
