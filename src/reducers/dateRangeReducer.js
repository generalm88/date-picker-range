import {
  UPDATE_START_DATE,
  UPDATE_END_DATE,
  STATUS_HIDE_SELECTED,
  DATE_FILTER,
  UPDATE_CURRENT_RANGE_START,
  UPDATE_CURRENT_RANGE_END,
  UPDATE_INIT_DATE,
} from "../actions/dateRangeActions";

const initialState = {
  initDate: {
    start: new Date(),
    end: new Date(),
  },
  currentRange: {
    start: null,
    end: null,
  },
  today: new Date(),
  hideSelected: true,
  dateFilter: null,
  lifetime: new Date("2017-04-04"),
};

export const dateRangeReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_START_DATE:
      return {
        ...state,
        initDate: { ...state.initDate, start: action.payload },
      };

    case UPDATE_END_DATE:
      return {
        ...state,
        initDate: { ...state.initDate, end: action.payload },
      };

    case UPDATE_CURRENT_RANGE_START:
      return {
        ...state,
        currentRange: { ...state.currentRange, start: action.payload },
      };

    case UPDATE_CURRENT_RANGE_END:
      return {
        ...state,
        currentRange: { ...state.currentRange, end: action.payload },
      };

    case STATUS_HIDE_SELECTED:
      return { ...state, hideSelected: action.payload };

    case DATE_FILTER:
      return { ...state, dateFilter: action.payload };

    case UPDATE_INIT_DATE:
      return { ...state, initDate: Object.assign({}, state.currentRange) };

    default:
      return state;
  }
};
