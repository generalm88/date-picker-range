import * as moment from "moment";
export const convertDatePicker = (initDate, today) => {
  const start = initDate.start;
  const end = initDate.end;
  let res;

  if (
    start.getMonth() === end.getMonth() &&
    start.getDate() === end.getDate() &&
    start.getFullYear() === end.getFullYear()
  ) {
    if (
      start.getMonth() === today.getMonth() &&
      start.getDate() === today.getDate() &&
      start.getFullYear() === today.getFullYear()
    ) {
      res = `${
        "Today, " +
        start.toLocaleString("en", { month: "short" }) +
        " " +
        start.getDate().toString()
      }`;
    } else {
      res = `${moment(end).format("ll")}`;
    }
  } else {
    if (
      start.getDate() === 1 &&
      start.getMonth() === end.getMonth() &&
      start.getFullYear() === end.getFullYear() &&
      end.getDate() === moment(end).endOf("month")["_d"].getDate()
    ) {
      res = `${
        start.toLocaleString("en", { month: "long" }) +
        ", " +
        start.getFullYear()
      }`;
      return res;
    }
    if (
      start.getDate() === 1 &&
      start.getMonth() === 0 &&
      start.getFullYear() === end.getFullYear() &&
      end.getDate() === 31
    ) {
      res = start.getFullYear();
      return res;
    }
    if (start.getFullYear() !== end.getFullYear()) {
      res = `${moment(start).format("ll") + " - " + moment(end).format("ll")}`;
      return res;
    }
    res = `${
      start.toLocaleString("en", { month: "short" }) +
      " " +
      start.getDate().toString() +
      " - " +
      moment(end).format("ll")
    }`;
  }
  return res;
};
