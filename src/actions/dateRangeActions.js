const UPDATE_START_DATE = "UPDATE_START_DATE";
const UPDATE_END_DATE = "UPDATE_END_DATE";
const STATUS_HIDE_SELECTED = "STATUS_HIDE_SELECTED";
const DATE_FILTER = "DATE_FILTER";
const UPDATE_CURRENT_RANGE_START = "UPDATE_CURRENT_RANGE_START";
const UPDATE_CURRENT_RANGE_END = "UPDATE_CURRENT_RANGE_END";
const UPDATE_INIT_DATE = "UPDATE_INIT_DATE";

const setStartDay = (value) => ({
  type: UPDATE_START_DATE,
  payload: value,
});

const setEndDay = (value) => ({
  type: UPDATE_END_DATE,
  payload: value,
});

const setHideSelected = (value) => ({
  type: STATUS_HIDE_SELECTED,
  payload: value,
});
const setDateFilter = (value) => ({
  type: DATE_FILTER,
  payload: value,
});
const setCurrentRangeStart = (value) => ({
  type: UPDATE_CURRENT_RANGE_START,
  payload: value,
});

const setCurrentRangeEnd = (value) => ({
  type: UPDATE_CURRENT_RANGE_END,
  payload: value,
});

const updateInitDate = () => ({
  type: UPDATE_INIT_DATE,
});

export {
  setStartDay,
  setEndDay,
  setHideSelected,
  setDateFilter,
  setCurrentRangeStart,
  setCurrentRangeEnd,
  updateInitDate,
  UPDATE_START_DATE,
  UPDATE_END_DATE,
  STATUS_HIDE_SELECTED,
  DATE_FILTER,
  UPDATE_CURRENT_RANGE_START,
  UPDATE_CURRENT_RANGE_END,
  UPDATE_INIT_DATE,
};
