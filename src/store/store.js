import { createStore, applyMiddleware, compose } from "redux";

import { dateRangeReducer } from "../reducers/dateRangeReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  dateRangeReducer,

  composeEnhancers(applyMiddleware())
);

store.subscribe(() => store.getState());

export default store;
